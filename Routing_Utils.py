#!/usr/bin/python3

import sys

class HashGraph:
    '''
        A Hash based adjacency matrix.

        The data within the matrix is also a hash of edge
        parameters - pdelay, link capacity, and consumed
        link resources
    '''
    def __init__( self ):
        self.matrix = {}
        self.nodes  = set()

    def addEdge( self, parent, child, pdelay, capacity ):
        # Won't duplicate nodes - set()
        self.nodes.add(parent)
        self.nodes.add(child)
        # Create the bidirectional edge
        self._link( parent, child, pdelay, capacity )

    def _link( self, parent, child, pdelay, capacity ):
        # Create the link in both directions
        self._insert_con( parent, child, pdelay, capacity )
        self._insert_con( child, parent, pdelay, capacity )

    def _insert_con( self, frm, to, pdelay, capacity ):
        # Create the edge in the adjacency matrix (3d dict)
        entry = { 'pdelay': pdelay, 'capacity': capacity, 'used': 0 }
        try:
            self.matrix[ frm ][ to ] = entry
        except KeyError:
            self.matrix[ frm ] = {}
            self.matrix[ frm ][ to ] = entry

    def getLoad( self, parent, child ):
        try:
            cap = self.matrix[ parent ][ child ][ 'capacity' ]
            con = self.matrix[ parent ][ child ][ 'used' ]
            # Return load
            return con/cap

        except ZeroDivisionError:
            # No capacity used
            return 0.0

    def freePath( self, path ):
        # Free unit capacity for an entire path
        tload = 0 # Used for consume/free debugging
        for i in range(len(path)):
            if i+1 < len(path):
                tload += self.freeEdge( path[i], path[i+1] )
        return tload

    def consumePath( self, path ):
        # Consume unit capacity for an entire path
        for i in range(len(path)):
            if i+1 < len(path):
                self.consumeEdge( path[i], path[i+1] )

    def totalLatency( self, path ):
        # Calculate the total latency for a path
        lat = 0
        for i in range(len(path)):
            if i+1 < len(path):
                lat += self.matrix[ path[i] ][ path[i+1] ][ 'pdelay' ]
        return lat

    def consumeEdge( self, parent, child ):
        # Return load if fully consumed
        load = self.getLoad( parent, child )
        if load >= 1.0: return 1.0
        # Consume for both edge directions
        self.matrix[ parent ][ child ][ 'used' ] += 1
        self.matrix[ child ][ parent ][ 'used' ] += 1
        return self.getLoad( parent, child )

    def freeEdge( self, parent, child ):
        # Only free if some capcity has been consumed
        if self.matrix[ parent ][ child ][ 'used' ] <= 0:
            # Return load, there might be no capacity to consume
            return 0.0
        # Free up unit capacity
        self.matrix[ parent ][ child ][ 'used' ] -= 1
        self.matrix[ child ][ parent ][ 'used' ] -= 1
        return self.getLoad( parent, child )


    def checkPath( self, path ):
        # Returns false if a hop on the path is at capacity
        for i in range(len(path)):
            if i+1 < len(path):
                if self.getLoad( path[i], path[i+1] ) >= 1.0:
                    return False
        # Otherwise True
        return True

    '''
        Dijkstra's for LeastHop and fastest (min pdelay) route.

        A bespoke cost evaluation routine is used with Dijkstr's
        for LeastLoad.

        self.bestPath( A, B ) ------------------ Fastest route from A to B (min pdelay)
        self.bestPath( A, B, LeastHops=True ) -- Least interum connections from A to B
        self.bestPath( A, B, LeastLoad=True ) -- Least congested path (min capcity/consumed)

        RETURNS:  cost, [ ordered path - including start & finish ]
    '''
    def bestPath( self, start, finish, alg='SDP'):

        if start  not in self.nodes or \
           finish not in self.nodes : return None, None

        curCost   = 0
        curNode   = start
        visited   = {}
        # Parent node hash representation
        parentRep = { start: None }
        toVisit   = { node : None for node in self.nodes }
        # No reflexive cost
        toVisit[ curNode ] = curCost

        while toVisit:
            # For ea adj node and cost from here to that node
            for adj, cost in self.matrix[ curNode ].items():
                if adj not in toVisit: continue
                # If least loaded path requested
                if alg == 'LLP':
                    # Link load = capacity/consumed
                    adjCost = self.getLoad( curNode, adj )
                    # Cost to adj is max of the total path
                    adjCost = max( curCost, adjCost )
                # If least hop or fastest path requested
                else:
                    # Calculate total cost from start to adj
                    if alg == 'SHP': adjCost = curCost + 1
                    else: adjCost = curCost + cost['pdelay']
                # If we've found a lower cost path to adj
                if toVisit[ adj ] is None or adjCost < toVisit[ adj ]:
                    # Update known distance and parent node
                    toVisit[ adj ]   = adjCost
                    parentRep[ adj ] = curNode
            # Record the final cost from start to curr
            visited[ curNode ] = curCost
            del toVisit[ curNode ]
            # Finish if nothing left to visit
            if not toVisit: break
            # Otherwise visit the nearest node on the todo list
            todo = [ node for node in toVisit.items() if node[1] != None ]
            todo = sorted(todo, key = lambda n: n[1])
            curNode, curCost = todo[0]
        # Solve shortest path
        path = []
        parent = parentRep[ finish ]
        path.append( finish )
        # Work back from finish node
        while parent:
            path.append( parent )
            parent = parentRep[ parent ]
        path.reverse()
        # Return cost and path as ordered list
        return visited[ finish ], path


def error( msg ):
    sys.stderr.write( msg+"\n" )
    exit(1)

def getArgs():
    # CALLING CONVENTION
    # ./RoutingPerformance.py (CIRCUIT|PACKET) (SHP|SDP|LLP) [TOPOLOGY_FILE] [WORKLOAD_FILE] [PACKET RATE]
    args = {}

    if len(sys.argv) != 6:
        print("Not enough arguments provided, the calling convention is;")
        print("./RoutingPerformance.py (CIRCUIT|PACKET) (SHP|SDP|LLP) [TOPOLOGY_FILE] [WORKLOAD_FILE] [PACKET RATE]")
        exit(1)

    connection = sys.argv[1]
    connection = connection.upper()
    if connection not in ['CIRCUIT', 'PACKET']:
        error("Connection type must be either CIRCUIT or PACKET")
    args['connection type'] = connection

    algorithm = sys.argv[2]
    algorithm = algorithm.upper()
    if algorithm not in ['SHP', 'SDP', 'LLP']:
        error("Routing algorithm must be one of SHP, SDP, & LLP")
    args['algorithm'] = algorithm

    topoFile = sys.argv[3]
    try:
        topology = open(topoFile, 'r')
    except FileNotFoundError:
        error("Cannot open "+topoFile+" - File not found")
    args['topology file'] = topology

    workFile = sys.argv[4]
    try:
        workload = open(workFile, 'r')
    except FileNotFoundError:
        error("Cannot open "+workFile+" - File not found")
    args['workload file'] = workload

    packetRate = sys.argv[5]
    try:
        packetRate = 1.0/float(packetRate)
    except:
        error("Packet rate must be a number")
    args['packet rate'] = packetRate

    return args






















if __name__ == "__main__":
    '''
         B___D
        /\   |\
       A  \  | F
        \  \ |/
         C___E

    The least delay path is ['A', 'C', 'E', 'D', 'F'] with a cost of 14
    The least loaded path is ['A', 'B', 'E', 'D', 'F'] with a cost of 0.4
    The least hop path is ['A', 'B', 'D', 'F'] with a cost of 3

    '''
    g = HashGraph()

    g.addEdge( 'A', 'B', 3,  4 )
    g.addEdge( 'A', 'C', 4,  4 )
    g.addEdge( 'C', 'E', 1,  4 )
    g.addEdge( 'B', 'E', 5,  4 )
    g.addEdge( 'B', 'D', 6,  4 )
    g.addEdge( 'D', 'E', 2,  4 )
    g.addEdge( 'D', 'F', 7,  4 )
    g.addEdge( 'E', 'F', 12, 4 )

    g.consumeEdge( 'A', 'B' ) # Load = 0.25
    g.consumeEdge( 'A', 'C' )
    g.consumeEdge( 'A', 'C' ) # Load = 0.50
    g.consumeEdge( 'C', 'E' ) # Load = 0.25
    g.consumeEdge( 'B', 'E' )
    g.consumeEdge( 'B', 'E' ) # Load = 0.50
    g.consumeEdge( 'B', 'D' )
    g.consumeEdge( 'B', 'D' )
    g.consumeEdge( 'B', 'D' )
    g.consumeEdge( 'B', 'D' ) # Load = 1.0
    g.consumeEdge( 'D', 'E' ) # Load = 0.25
    g.consumeEdge( 'D', 'F' ) # Load = 0.25
    g.consumeEdge( 'E', 'F' )
    g.consumeEdge( 'E', 'F' )
    g.consumeEdge( 'E', 'F' ) # Load = 0.75


    #
    # Check fastest path
    #
    cost, path = g.bestPath( 'A', 'F' )
    if not cost or not path:
        print( "No path was found" )
    else:
        print( "The least delay path\n", " => ".join(path), "with a delay of", cost )
        print("Is it OK to use this path?", g.checkPath(path), "\n")

    #
    # Check least load path
    #
    alg = 'LLP'
    cost, path = g.bestPath( 'A', 'F', alg )
    if not cost or not path:
        print( "No path was found" )
    else:
        print( "The least loaded path is\n", " => ".join(path), "with a max load of", cost)
        print("Is it OK to use this path?", g.checkPath(path), "\n")

    #
    # Check shortest hop path
    #
    alg = 'SHP'
    cost, path = g.bestPath( 'A', 'F', alg )
    if not cost or not path:
        print( "No path was found" )
    else:
        print( "The least hop path is\n", " => ".join(path), cost, "Hops")
        print("Is it OK to use this path?", g.checkPath(path), "\n")

    #
    # Check a fully loaded path
    #
    badLLPath = ['A', 'B', 'D', 'F']
    print("Is it OK to use a fully loaded path?", g.checkPath(badLLPath), "\n")


    #
    # Check loading / unloading an edge
    #
    #print("Testing edge consumption / free...")
    #print("The current load from edge A to C is ", g.getLoad('A', 'C'))       # 0.5
    #print("Consuming one link unit makes it ", g.consumeEdge('A', 'C'))       # 0.75
    #print("Consuming one link unit makes it ", g.consumeEdge('A', 'C'))       # 1.0
    #print("Consuming one link unit makes it ", g.consumeEdge('A', 'C'))       # 1.0
    #print("Freeing one link unit makes it ", g.freeEdge('A', 'C'))            # 0.75
    #print("Freeing one link unit makes it ", g.freeEdge('A', 'C'))            # 0.50
    #print("Freeing one link unit makes it ", g.freeEdge('A', 'C'))            # 0.25
    #print("Freeing one link unit makes it ", g.freeEdge('A', 'C'))            # 0.0
    #print("Freeing one link unit makes it ", g.freeEdge('A', 'C'))            # 0.0

    #
    # Test loading/unloading an entire path
    #
    #_, path = g.bestPath( 'A', 'F', 'LLP' )
    #g.consumePath( path )
    #g.consumePath( path )
    #g.consumePath( path )
    #g.consumePath( path )
    #g.freePath( path )
    #g.freePath( path )
    #g.freePath( path )
    #g.freePath( path )

