#!/usr/bin/python3

from Routing_Utils import HashGraph, getArgs, error
import numpy as np
import sys, re

# Get CMD ARGs
args = getArgs()
topo = args['topology file']
work = args['workload file']
conn = args['connection type']
algo = args['algorithm']
rate = args['packet rate']
# Create the Graph
networkGraph = HashGraph()
# Assemble the network topology using REGEX
for line in topo:
    match = re.match("([A-Z]) +([A-Z]) +([0-9]+) +([0-9]+)", line)
    if match:
        networkGraph.addEdge(match.group(1),      match.group(2),\
                         int(match.group(3)), int(match.group(4)))

finish_time = 0
worklist    = []
# Assemble worklist from workload file
for line in work:
    match = re.match("^([0-9.]+) +([A-Z]) +([A-Z]) +([0-9.]+)$", line)
    if match:
        connection_time = float(match.group(1))
        teardown_time   = int((float(match.group(4)) + connection_time)*1e6)
        connection_src  = match.group(2)
        connection_dst  = match.group(3)
        # Store time in usec
        worklist.append( (int(connection_time*1e6), connection_src, \
                  connection_dst,           teardown_time) )
        # Store the finish time in usec
        if teardown_time > finish_time:
            finish_time  = teardown_time

# Sort the worklist
worklist = sorted(worklist, key=lambda n: n[0])

requests = 0
blocked  = 0
routed   = 0
dropped  = 0
avghops  = []
avgdelay = []
packets  = []


def gen_eid( eid ):
    while True:
        yield eid
        eid += 1

def npackets( duration ):
    pps = 1/rate;
    return int((duration/1e6)*pps)


eid = gen_eid(1)

teardown = []
# Iterate over microseconds
for microsecond in range(finish_time + 1):
    # While there's connections to be made at this time in the worklist
    while( worklist and microsecond == worklist[0][0] ):
        requests += 1
        # Find the best path src -> dst for the given algorithm
        cost, path = networkGraph.bestPath( worklist[0][1], worklist[0][2], algo)
        pathOk = networkGraph.checkPath( path )
        # Consume path if ok
        if pathOk:
            networkGraph.consumePath( path )
            avghops.append( len(path) - 1 )
            avgdelay.append( networkGraph.totalLatency(path) )

        duration = worklist[0][3]-worklist[0][0]
        # If it's a circuit connection, log the teardown time
        if conn == "CIRCUIT":
            if pathOk:
                teardown.append( (worklist[0][3], path) )
                # Sort the teardown times
                teardown = sorted(teardown, key=lambda x: x[0] )
                routed += npackets(duration)
            else:
                dropped += npackets(duration)
        # If it's a packet connection
        if conn == "PACKET":
            npkts = npackets(duration) or 1
            # Create the teardown times [teardown time, path, renegotiate, free]
            pkts = [ [microsecond + x*rate*1e6, path, True, True, next(eid)] for x in range(1, npkts+1)  ]
            # Create the final teardown - no renegotiate on final teardown
            pkts.append( [worklist[0][3], path, False, True, next(eid)] )
            if pathOk:
                routed+=1
            # If initial packet dropped (path not ok)
            else: 
                dropped += 1
                # Mark first teardown, no free, path wasn't consumed
                pkts[0][3] = False
            teardown += pkts
            # Sort teardown times
            teardown = sorted(teardown, key=lambda x: x[0] )
        # Remove worklist entry
        del(worklist[0])


    # While there's teardowns to be made at this time
    while( teardown and microsecond == teardown[0][0] ):
        # If it's a circuit connection
        if conn == "CIRCUIT":
            # Free unit resource on the path
            networkGraph.freePath( teardown[0][1] )
        # If it's a packet connection
        elif conn == "PACKET":
            # Free the path if we're supposed to
            if( teardown[0][3] ):
                networkGraph.freePath( teardown[0][1] )
            # Renegotiate the path if we're supposed to
            if( teardown[0][2] ):
                cost, path = networkGraph.bestPath( teardown[0][1][0], teardown[0][1][-1], algo)
                pathOk = networkGraph.checkPath( path )
                # Consume path if ok
                if pathOk:
                    networkGraph.consumePath( path )
                    avghops.append( len(path) - 1 )
                    avgdelay.append( networkGraph.totalLatency(path) )
                    routed += 1
                # Otherwise
                else:
                    dropped += 1
                    # Mark next teardown - no free
                    for entry in teardown:
                        if entry[4] == teardown[0][4]+1:
                            entry[3] = False
                            break
        # Remove this teardown entry
        del( teardown[0] )


print("Requests : ", requests)
print("N pkts   : ", routed+dropped)
print("Routed   : ", routed)
print("P routed : ", (routed/(routed+dropped)*100))
print("Dropped  : ", dropped)
print("P dropped: ", (dropped/(routed+dropped)*100))
print("Avg hops : ", sum(avghops)/len(avghops))
print("Avg delay: ", sum(avgdelay)/len(avgdelay))

